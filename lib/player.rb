class HumanPlayer
	def initialize(name)
		@name = name
	end

	def get_play
		puts "Enter target square"
		gets.chomp.split(", ").map(&:to_i)
	end
end
